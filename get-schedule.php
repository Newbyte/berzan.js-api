<?php
header('Access-Control-Allow-Origin: *');
ini_set('allow_url_fopen', 1);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if ($_GET['filetype'] === 'png' || $_GET['filetype'] === 'gif') {
        header('Content-Type: image/' . $_GET['filetype']);
    } else {
        invalidRequest();
    }

    echo file_get_contents(generateScheduleURL());
} else {
    invalidRequest();
}

function generateScheduleURL(): string
{
    $filetype = sanitise('filetype');
    $class = sanitise('class');
    $week = sanitise('week');
    $weekDay = sanitise('weekDay');
    $language = sanitise('lang');

    $scheduleWidth = sanitise('width');
    $scheduleHeight = sanitise('height');

    return 'http://www.novasoftware.se/ImgGen/schedulegenerator.aspx?format=' . $filetype . '&schoolid=89920/' . $language . '&type=-1&id=' . $class . '&period=&week=' . $week . '&mode=0&printer=0&colors=32&head=0&clock=0&foot=0&day=' . $weekDay . '&width=' . $scheduleWidth . '&height=' . $scheduleHeight;
}

function sanitise(string $key): string
{
    $value = filter_input(INPUT_GET, $key, FILTER_SANITIZE_URL);

    if (is_null($value)) {
        invalidRequest();
    }

    return $value;
}

function invalidRequest(): void
{
    http_response_code(400);
    echo 'Invalid request';
    die;
}