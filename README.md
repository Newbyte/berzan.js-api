# Berzan.js Wrapper API
## What's this?

This repository contains a wrapper API that wraps [Skolmaten](https://skolmaten.se/)'s lunch API and [Novasoftware/Skola24](https://www.novasoftware.se/https://www.novasoftware.se/)'s schedule API for Berzeliusskolan in one package. It has the benefit of that it can be self-hosted, which means it can be used over HTTPS and fetched from any origin you choose. 

## Availability

Currently it is hosted by Heroku at [berzan-api.herokuapp.com](https://berzan-api.herokuapp.com/) — feel free to use it in your own web apps! It is also, (perhaps ironically) optionally, used by [Berzan.js](https://berzan.js.org/).

## Documentation

Documentation will soon be available at [Berzan Docs](https://berzan-docs.herokuapp.com/).
